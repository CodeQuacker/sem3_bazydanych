-- 1. Dla ka�dego projektu poda� liczb� jego realizacji.
SELECT proname, COUNT(*) LR
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
GROUP BY proname;

SELECT proname,
       (
           SELECT COUNT(1)
           FROM implproject ip
           WHERE p.prono = ip.prono) LR
FROM project p;

-- 2. Poda� list� pracownik�w oraz liczb� realizowanych przez nich projekt�w.
SELECT ename,
       (
           SELECT COUNT(1)
           FROM implemp ie
           WHERE ie.empno = e.empno
       )
FROM emp e
ORDER BY 1;

-- 3. Poda� nazwy departament�w z kt�rych bierze udzia� w projektach najwi�cej pracownik�w.
SELECT d.dname
FROM dept d,
     (
         SELECT deptno
         FROM emp e,
              implemp ie
         WHERE e.empno = ie.empno
         GROUP BY deptno
         HAVING COUNT(1) = (
             SELECT MAX(COUNT(1))
             FROM implemp ie1,
                  emp e1
             WHERE e1.empno = ie1.empno
             GROUP BY deptno
         )
     ) pod
WHERE d.deptno = pod.deptno;

-- 4. Poda� w tym samym wierszu nazw� projektu z najwi�kszym oraz najmniejszym bud�etem.
SELECT a.proname MAX, b.proname MIN
FROM (
         SELECT proname
         FROM project p
         WHERE p.budget = (
             SELECT MAX(budget)
             FROM project
         )
     ) a,
     (
         SELECT proname
         FROM project p
         WHERE p.budget = (
             SELECT MIN(budget)
             FROM project
         )
     ) b;


-- 5. Dla ka�dego departamentu poda� jaki procent stanowi� pracownicy oraz ich zarobki, bior�c pod uwag� wszystkie departamenty.
SELECT a.dname                                          "Depart",
       100 * ROUND(a.l_prac / b.calk_licz, 2) || '%' AS "%_Empl",
       100 * ROUND(a.sal_sum / b.calk_sum, 2) || '%' AS "%_Salary"
FROM (
         SELECT dname, d.deptno, COUNT(*) AS l_prac, SUM(sal + NVL(comm, 0)) AS sal_sum
         FROM emp e,
              dept d
         WHERE d.deptno = e.deptno
         GROUP BY d.deptno, dname
     ) a,
     (
         SELECT COUNT(*) AS calk_licz, SUM(sal + NVL(comm, 0)) AS calk_sum
         FROM emp e
         WHERE deptno IS NOT NULL
     ) b;

-- NIESKONCZONE - poprosi� o skrypt nauczyciela
SELECT NVL(a.dname, "BRAK")                             "Depart",
       100 * ROUND(a.l_prac / b.calk_licz, 2) || '%' AS "%_Empl",
       100 * ROUND(a.sal_sum / b.calk_sum, 2) || '%' AS "%_Salary"
FROM (SELECT dname, d.deptno, COUNT(*) AS l_prac, SUM(sal + NVL(comm, 0)) AS sal_sum
      FROM emp e,
           dept d
      WHERE d.deptno = e.deptno
      GROUP BY d.deptno, dname) a,
     (SELECT COUNT(*) AS calk_licz, SUM(sal + NVL(comm, 0)) AS calk_sum
      FROM emp e
      WHERE deptno IS NOT NULL) b;

-- 6. Poda� nazwiska trzech pracownik�w najcz�ciej realizuj�cych projekty.
SELECT ename, COUNT(1)
FROM emp e,
     implemp ie
WHERE e.empno = ie.empno
GROUP BY e.empno, ename
ORDER BY 2 DESC;

SELECT *
FROM (
         SELECT ename, COUNT(1)
         FROM emp e,
              implemp ie
         WHERE e.empno = ie.empno
         GROUP BY e.empno, ename
         ORDER BY 2 DESC)
WHERE rownum <= 3;

SELECT o.ename
FROM (
         SELECT ename, COUNT(1)
         FROM emp e,
              implemp ie
         WHERE e.empno = ie.empno
           AND rownum <= 3
         GROUP BY e.empno, ename
         ORDER BY 2 DESC) o;

-- 7. Poda� dwa najrzadziej realizowane projekty.
SELECT proname
FROM (
         SELECT proname, COUNT(1)
         FROM project p,
              implproject ip
         WHERE p.prono = ip.prono
         GROUP BY p.prono, proname
         ORDER BY 2)
WHERE rownum >= 1
  AND rownum <= 2;

-- 8. Poda� warto�� realizowanych projekt�w w 2008 roku.


-- 9. Poda� warto�ci realizowanych projekt�w w poszczeg�lnych latach realizacji.


-- 10. Poda� nazw� projektu realizowanego w najkr�tszym czasie.
-- 11. Poda� nazwy projekt�w nie realizowanych.
-- 12. W jednym wierszu poda� sumaryczne zarobki pracownik�w na poszczeg�lnych stanowiskach oraz liczb� pracownik�w nie posiadaj�cych bonus�w uwzgl�dniaj�c stanowisko i departament.