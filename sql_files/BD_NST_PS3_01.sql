--1. Za pomoc? polecenia DESC wy?wietli? struktury tabel: EMP, DEPT, SALGRADE.
DESC emp;
DESC dept;
DESC salgrade;

--2. Wy?wietli? wszystkie dane z tabel (2 sposoby).
SELECT *
FROM emp;
SELECT *
FROM dept;
SELECT *
FROM salgrade;
--  2 spos�b
SELECT empno,
       JOB,
       ename,
       sal,
       comm,
       mgr,
       deptno,
       hiredate
FROM emp;
SELECT deptno, loc, dname
FROM dept;
SELECT grade, hisal, losal
FROM salgrade;

--3. Poda? nazwy departament�w.
SELECT dname
FROM dept;

SELECT DISTINCT JOB
FROM emp;
--4. Poda? wszystkie, wzajemnie r�?ne stanowiska pracy.

--5. Poda? numery i nazwiska os�b zarabiaj?cych 2500 oraz nie wi?cej ni? 3200.
SELECT empno, ename, sal
FROM emp
WHERE sal BETWEEN 2500 AND 3200;
--WHERE SAL>=2500 AND SAL<=3200;

--6. Poda? nazwiska i daty zatrudnienia pracownik�w na stanowiskach : SALESMAN, MANAGER, CLERK.
SELECT ename, hiredate
FROM emp
WHERE JOB IN ('SALESMAN', 'MANAGER', 'CLERK');

--7. Dla ka?dego pracownika poda? sum? jego p?acy i zarobk�w.
SELECT EMPNO, ENAME, SAL + COMM, SAL + NVL(COMM, 0)
FROM EMP;
--SAL+COMM to number + number lub number + null. Number + null zawsze zwraca null. Funkcja NVL

--8. Poda? dane o pracownikach z departament�w 10 i 20 w kolejno?ci alfabetycznej wg nazwisk i malej?cej wg departamentu.
SELECT ENAME, DEPTNO
FROM EMP
WHERE DEPTNO IN (10, 20) --DEPTNO=10 OR DEPTNO=20
ORDER BY 1, 2 DESC;
--ENAME, DEPTNO DESC;

--9. Wybra? informacje o pracownikach, kt�rzy nie posiadaj? zwierzchnika.
SELECT ENAME
FROM EMP
WHERE MGR IS NULL;
--10. Poda? numer, nazwisko, stanowisko i wynagrodzenie pracownik�w na stanowisku SALESMAN z zarobkami powy?ej 1500 lub na stanowisku CLERK.
SELECT EMPNO, ENAME, JOB, SAL
FROM EMP
WHERE JOB = 'SALESMAN' AND SAL > 1500
   OR JOB = 'CLERK';

--11. Poda? nazwiska, nazwy departament�w pracownik�w, kt�rych departamenty nie mieszcz? si? w Chicago. Doda? alias dla pola nazwisko i dwucz?onowy dla nazwy departamentu.
SELECT ENAME Name, DNAME "Departament name"
FROM EMP,
     DEPT
WHERE EMP.DEPTNO = DEPT.DEPTNO
  AND NOT LOC != 'CHICAGO';
--LOC='CHICAGO';

--12. Poda? nazwiska pracownik�w, kt�rych zarobki s? wi?ksze ni? przynajmniej jednego pracownika z departamentu 10.
SELECT E1.ENAME, E1.SAL, E2.ENAME, E2.SAL
FROM EMP E1,
     EMP E2
WHERE E1.SAL > E2.SAL
  AND E2.DEPTNO = 10
ORDER BY 1;

--13. Poda? nazwiska pracownik�w, kt�rych zarobki powi?kszone o 25% przekraczaj? 3000.
SELECT ename, sal, sal * 1.25 PP
FROM emp
WHERE sal * 1.25 > 3000;

--14. Ile miesi?cy pracuj? poszczeg�lni pracownicy.
SELECT ename, TRUNC(MONTHS_BETWEEN(SYSDATE, hiredate)) AS time, MONTHS_BETWEEN(SYSDATE, hiredate)
FROM emp;

--15. Poda? odpowied?, w postaci zdania, kt�ry pracownik nie posiada zwierzchnika.
SELECT 'Pracownik o nazwisku ' || ename || ' nie ma zwierzchnika ' zdanie
FROM emp
WHERE mgr IS NULL;

--16. Dla ka?dego pracownika poda? d?ugo?? jego nazwiska.
SELECT ename, LENGTH(ename)
FROM emp;

--17. Poda? nazwiska pracownik�w zapisane z du?ej litery a stanowiska zapisane ma?ymi literami.
SELECT INITCAP(ename), LOWER(job)
FROM emp;

--18. Poda? trzy sposoby wyszukania pracownika o nazwisku rozpoczynaj?cym si? od liter BL.
SELECT ename
FROM emp
WHERE ename LIKE 'BL%';

SELECT ename
FROM emp
WHERE SUBSTR(ename, 1, 2) = 'BL';

SELECT ename
FROM emp
WHERE INSTR(INITCAP(ename), 'Bl', 1) = 1;