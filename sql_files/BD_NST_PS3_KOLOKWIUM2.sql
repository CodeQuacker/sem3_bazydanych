-- Poda� nazwiska i imiona w�a�cicieli samochod�w z cen� powy�ej �redniej ceny wszystkich samochod�w.
SELECT w.nazwisko, w.imie
FROM wlasciciele w
         JOIN samochody s USING (idwlasciciela)
WHERE s.cena > (SELECT AVG(cena) FROM samochody);

-- Kt�rzy (nazwa firmy) dealerzy z Bialystok posiadaj� marki samochod�w wyst�puj�ce u dealer�w z Sokolka.
SELECT d.nazwafirmy
FROM dealerzy d
         JOIN samochody s USING (iddealera)
WHERE UPPER(d.miasto) = 'BIALYSTOK'
  AND s.idsamochodu IN (SELECT s1.idsamochodu
                        FROM dealerzy d1
                                 JOIN samochody s1 USING (iddealera)
                        WHERE UPPER(d1.miasto) = 'SOKOLKA');

-- Poda� nazwy marek samochod�w, dla kt�rych nie istniej� modele (po��czenie zewn�trzne).
SELECT mr.nazwa
FROM modele mo
         FULL OUTER JOIN marki mr USING (idmarki)
WHERE mo.idmodelu IS NULL;

-- Poda� nazwiska klient�w, kt�rzy nie podpisywali um�w (z predykatem NOT EXISTS).
SELECT k.nazwisko
FROM klienci k
WHERE NOT EXISTS(SELECT NULL
                 FROM umowy u
                 WHERE k.idklienta = u.idklienta);

-- SELECT *
-- FROM klienci LEFT JOIN umowy USING (idklienta)
-- where idumowy is NULL ;

-- Poda� id samochod�w, dat� wstawienia z wstawionym wykrzyknikiem w wierszu samochodu najwcze�niej wstawionego. Uporz�dkowa� wg daty wstawienia (operatory zbior�w).
SELECT idsamochodu, data_wstawienia || '  '
FROM samochody
WHERE data_wstawienia != (SELECT MIN(data_wstawienia) FROM samochody)
UNION
SELECT idsamochodu, data_wstawienia || ' !'
FROM samochody
WHERE data_wstawienia = (SELECT MIN(data_wstawienia) FROM samochody)
ORDER BY 2;
