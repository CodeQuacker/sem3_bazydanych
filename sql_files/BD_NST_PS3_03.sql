--1. Poda� liczb� pracownik�w w tabeli EMP.
SELECT COUNT(*)
FROM emp;

SELECT job
FROM emp
WHERE empno IN (
    SELECT DISTINCT mgr
    FROM emp
);


--2. Poda� min, max, avg, sum pensj� w firmie.
SELECT MIN(sal), MAX(sal), TRUNC(AVG(sal), 2), SUM(sal)
FROM emp;

--3. Obliczy� ilu pracownik�w jest kierownikami.
SELECT COUNT(DISTINCT mgr)
FROM emp;

--4. Poda� min, max pensj� dla pracownik�w z departamentu Research.
SELECT MIN(sal), MAX(sal)
FROM emp E,
     dept D
WHERE E.deptno = D.deptno
  AND INITCAP(dname) = 'Research';

--5. Poda� liczb� pracownik�w ze stopniem wynagrodzenia 1.
SELECT COUNT(*)
FROM emp,
     salgrade
WHERE grade = 1
  AND sal BETWEEN losal AND hisal;
WHERE grade=1 AND sal<=hisal AND sal>=losal;

--6. Poda� liczb� pracownik�w z premi� oraz sumaryczn� warto�� premii pracownik�w z departamentu z lokalizacj� w Dallas.
SELECT COUNT(*), SUM(NVL(comm, 0))
FROM emp e,
     dept d
WHERE comm IS NOT NULL
  AND e.deptno = d.deptno
  AND INITCAP(dname) = 'Dallas';

--7. Poda� min, max pensj� dla ka�dego stanowiska.
SELECT job, MIN(sal), MAX(sal)
FROM emp
GROUP BY job
ORDER BY 1;

--8. Obliczy� ilu pracownik�w posiada poszczeg�lne stopnie wynagrodzenia.
SELECT grade, COUNT(*)
FROM emp e,
     salgrade s
WHERE sal BETWEEN losal AND hisal
GROUP BY grade;

--9. Poda� dla ka�dego departamentu sum� zarobk�w i premii jego pracownik�w.
SELECT dname, SUM(sal + NVL(comm, 0))
FROM emp e,
     dept d
WHERE e.deptno = d.deptno
GROUP BY d.deptno, dname;
--grupujemy po deptno poniewa� nazwa dname mo�e si� powt�rzy� (np w przypadku imion i nazwisk)

--10. Poda� stanowiska, dla kt�rych max zarobki s� <2500.
SELECT job, MAX(sal)
FROM emp
GROUP BY job
HAVING MAX(sal) < 2500;

--11. Poda� nazwy departament�w dla kt�rych liczba pracownik�w na poszczeg�lnych stanowiskach przekracza 2 osoby.
SELECT job, dname, COUNT(1)
FROM emp e,
     dept d
WHERE e.deptno = d.deptno
GROUP BY job, d.deptno, dname
HAVING COUNT(1) > 2;

--12. Poda� stopie� wynagrodzenia z liczb� pracownik�w od 2 do 5.
SELECT grade, COUNT(*) IL_PRAC
FROM emp e,
     salgrade s
WHERE sal BETWEEN losal AND hisal
GROUP BY grade
HAVING COUNT(*) BETWEEN 2 AND 5;

--13. Poda� nazwy stanowisk dla kt�rych r�nica mi�dzy maksymalnymi i minimalnymi zarobkami nie przekracza 1000.
SELECT job
FROM emp
GROUP BY job
HAVING MIN(sal) + 1000 >= MAX(sal);

--14. Poda� stanowiska, na kt�rych pracuje 3 pracownik�w ze stopniem wynagrodzenia 1.
SELECT job
FROM emp e,
     salgrade s
WHERE grade = 1
  AND sal BETWEEN losal AND hisal
GROUP BY JOB
HAVING COUNT(1) = 3;

--15. Kt�re stanowiska w kt�rych dzia�ach s� obsadzone przez dw�ch lub wi�cej pracownik�w. Uporz�dkowa� wg liczby pracownik�w.
SELECT job, COUNT(1) il_pracownikow
FROM emp
GROUP BY job
HAVING COUNT(1) > 1
ORDER BY 2 DESC;

--16. Poda� sum� bonus�w uzyskanych przez pracownik�w departamentu 10.
SELECT COUNT(*), bonus
FROM emp e,
     bonus b
WHERE e.empno = b.empno
  AND deptno = 10
  AND bonus IS NOT NULL
  AND bonus != 0
GROUP BY bonus;

--17. Dla ka�dej nazwy projektu poda� liczb� realizacji w 2015 roku.
SELECT proname, COUNT(*)
FROM implproject ip,
     project p
WHERE ip.prono = p.prono
  AND EXTRACT(YEAR FROM start_date) = 2015
  AND EXTRACT(YEAR FROM end_date) = 2015
GROUP BY p.proname;

--18. Dla ka�dego departamentu poda� nazwy realizowanych projekt�w i liczb� realizacji.
SELECT dname, proname, COUNT(2)
FROM emp e,
     dept d,
     implemp ie,
     implproject ip,
     project p
WHERE e.empno = ie.empno
  AND ie.impl = ip.impl
  AND ip.prono = p.prono
  AND d.deptno = e.deptno
GROUP BY dname, proname
ORDER BY dname;

--19. Dla ka�dego stanowiska w departamencie poda� maksymalne bonusy pracownik�w.
SELECT dname, job, MAX(bonus)
FROM emp e,
     dept d,
     bonus b
WHERE e.deptno = d.deptno
  AND e.empno = b.empno
GROUP BY dname, job
ORDER BY dname;

--20. Dla ka�dej nazwy projektu poda� minimaln� sumaryczn� warto�� bud�etu realizacji.
SELECT proname, MIN(budget)
FROM project
GROUP BY proname;