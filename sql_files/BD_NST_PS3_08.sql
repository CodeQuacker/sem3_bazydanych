-- 1. Utworzy� tabele:
--     NAUCZ_AK(ID (pk), NAZWISKO (not null), TYTUL (not null)),
--     PRZEDMIOT(ID (pk), NAZWA (not null)),
--     ZAJECIA(ID (pk), TYP (ograniczenie warto�ci), ID_NAUCZ (not null),
--     ID_PRZEDM (not null))
CREATE TABLE naucz_ak_az
(
    id       NUMBER(2)
        CONSTRAINT pk_naucz_ak PRIMARY KEY,
    nazwisko VARCHAR2(15) NOT NULL,
    tytul    VARCHAR2(20) NOT NULL
);

CREATE TABLE przedmiot_az
(
    id    NUMBER(2)
        CONSTRAINT pk_naucz_az PRIMARY KEY,
    nazwa VARCHAR2(15) NOT NULL
);

CREATE TABLE zajecia_az
(
    typ       VARCHAR2(15)
        CONSTRAINT z_t_az CHECK (typ IN ('LAB', 'PS', 'S', 'W', 'CW', 'LEK', 'ZJ')),
    id_naucz  NUMBER(2) NOT NULL
        CONSTRAINT z_n_az REFERENCES naucz_ak_az (id),
    id_przedm NUMBER(2) NOT NULL,
    CONSTRAINT z_p_az FOREIGN KEY (id_przedm) REFERENCES przedmiot_az (id),
    CONSTRAINT zz_pk_az PRIMARY KEY (id_naucz, id_przedm)
);

ALTER TABLE zajecia_az
    DROP CONSTRAINT z_p_az;

-- 2. Okre�li� ograniczenia integralno�ci dla tabel (trzy sposoby).
ALTER TABLE zajecia_az
    ADD CONSTRAINT z_p_az FOREIGN KEY (id_przedm) REFERENCES przedmiot_az (id);

-- 3. Na bazie tabeli NAUCZ_AK utworzy� now� tabel� z dwoma polami.
CREATE TABLE naucz_ak2_az
AS
SELECT *
FROM naucz_ak_az;

-- 4. Zmodyfikowa� pole TYTUL (warto�� domy�lna).
ALTER TABLE naucz_ak_az
    MODIFY tytul DEFAULT 'MGR';

-- 5. Do tabeli NAUCZ_AK doda� pole stanowisko z ograniczeniem warto�ci.
ALTER TABLE naucz_ak_az
    ADD (stanowisko VARCHAR2(15), CHECK ( stanowisko IN ('ASYSTENT', 'ADIUNKT', 'PROFESOR') ));

-- 6. Wprowadzi� rekordy do tabel.
INSERT INTO naucz_ak_az (id, nazwisko, tytul, stanowisko)
VALUES (1, 'Nowak', 'PROF.', 'PROFESOR');
INSERT INTO naucz_ak_az (id, tytul, nazwisko, stanowisko)
VALUES (2, 'DR', 'Kowal', 'ADIUNKT');
INSERT INTO naucz_ak_az (id, nazwisko, stanowisko)
VALUES (3, 'Nowy', 'ASYSTENT');
INSERT INTO naucz_ak_az (id, nazwisko, tytul, stanowisko)
VALUES (4, 'Duda', 'DR', 'PROFESOR');

-- DELETE
-- FROM naucz_ak_az
-- WHERE id IN (SELECT id FROM naucz_ak_az);
-- SELECT *
-- FROM naucz_ak_az;

INSERT INTO przedmiot_az (id, nazwa)
VALUES (1, 'MATEMATYKA');
INSERT INTO przedmiot_az (id, nazwa)
VALUES (2, 'ANALIZA');
INSERT INTO przedmiot_az (id, nazwa)
VALUES (3, 'BAZY DANYCH');
INSERT INTO przedmiot_az (id, nazwa)
VALUES (4, 'SIECI KOMP');

INSERT INTO zajecia_az (typ, id_naucz, id_przedm)
VALUES ('LAB', '1', '2');
INSERT INTO zajecia_az (typ, id_naucz, id_przedm)
VALUES ('PS', '2', '4');
INSERT INTO zajecia_az (typ, id_naucz, id_przedm)
VALUES ('W', '3', '1');
INSERT INTO zajecia_az (typ, id_naucz, id_przedm)
VALUES ('CW', '4', '3');

COMMIT;
-- 7. Usun�� rekordy tabeli ZAJECIA dla nazwy przedmiotu Bazy danych.
DELETE
FROM zajecia_az
WHERE id_przedm IN (SELECT id FROM przedmiot_az WHERE nazwa = 'BAZY DANYCH');

-- 8. Do tabeli NAUCZ_AK doda� pole staz (przedzia� warto�ci 1-40).
ALTER TABLE naucz_ak_az
    ADD (staz NUMBER(2), CONSTRAINT n_staz_az CHECK ( staz BETWEEN 1 AND 40));

-- 9. Wprowadzi� warto�ci do pola staz.
SELECT *
FROM naucz_ak_az;
UPDATE naucz_ak_az
SET staz=4;
UPDATE naucz_ak_az
SET staz=1
WHERE id = 1;

-- 10. Osobie o podanym identyfikatorze, jako parametr, zmieni� tytu� naukowy.
UPDATE naucz_ak_az
SET tytul='PROF'
WHERE id = &T;
SELECT *
FROM naucz_ak_az;

-- 11. Na podstawie tabeli PRZEDMIOT utworzy� now� tabel�.
CREATE TABLE przedmiot3_az
AS
SELECT *
FROM przedmiot_az;

-- 12. Zmieni� nazw� nowoutworzonej tabeli.
RENAME przedmiot3_az TO przedmiot2_az;

-- 13. Osobie o podanym id, jako parametr, podnie�� sta� o 10%.
ALTER TABLE naucz_ak_az
    MODIFY staz NUMBER(4, 2);
UPDATE naucz_ak_az
SET staz=staz * 1.1
WHERE id = &E;

SELECT *
FROM naucz_ak_az;

-- 14. Na podstawie tabeli PRZEDMIOT utworzy� now� tabel�.
CREATE TABLE przedmiot3_az
AS
SELECT *
FROM przedmiot_az;

-- 15. Zmieni� nazw� nowoutworzonej tabeli REAL_PRZEDM.
RENAME real_przedm TO real_przedm_az;

-- 16. Usun�� rekordy z nowej tabeli.
DELETE
FROM real_przedm_az;

-- 17. Usun�� kolumn� nazwa pozostawiaj�c id.
ALTER TABLE real_przedm_az
    DROP COLUMN nazwa;

-- 18. Doda� pole LiczbaRealizacji do tabeli REAL_PRZEDM.
ALTER TABLE real_przedm_az
    ADD (liczbarealizacji NUMBER(3));

-- 19. Policzy� liczb� realizacji przedmiot�w na podstawie tabeli ZAJECIA i wprowadzi� do pola LiczbaRealizacji tabeli REAL_PRZEDM.
SELECT typ, nazwa, nazwisko, tytul, stanowisko, staz, rpa.liczbarealizacji Liczba_realizacji
FROM zajecia_az
         LEFT JOIN przedmiot_az pa ON zajecia_az.id_przedm = pa.id
         LEFT JOIN naucz_ak_az naa ON zajecia_az.id_naucz = naa.id
         LEFT JOIN real_przedm_az rpa ON zajecia_az.id_przedm = rpa.id;

INSERT INTO real_przedm_az (id, liczbarealizacji)
VALUES (1, (SELECT COUNT(*) FROM zajecia_az WHERE zajecia_az.id_przedm = 1));
INSERT INTO real_przedm_az (id, liczbarealizacji)
VALUES (2, (SELECT COUNT(*) FROM zajecia_az WHERE zajecia_az.id_przedm = 2));
INSERT INTO real_przedm_az (id, liczbarealizacji)
VALUES (3, (SELECT COUNT(*) FROM zajecia_az WHERE zajecia_az.id_przedm = 3));
INSERT INTO real_przedm_az (id, liczbarealizacji)
VALUES (4, (SELECT COUNT(*) FROM zajecia_az WHERE zajecia_az.id_przedm = 4));
COMMIT;