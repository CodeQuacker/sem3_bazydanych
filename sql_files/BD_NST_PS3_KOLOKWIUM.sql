--Poda� nazwiska, imiona i stanowisko pracownik�w obs�uguj�cych umowy podpisane w miesi�cu listopadzie 2018 roku z klientami o nazwisku na liter� K i imieniu na liter� M.
select imie, nazwisko, nazwa
from pracownicy p, stanowiska s, umowy u
where p.idpracownika=u.idpracownika and p.idstanowiska=s.idstanowiska AND imie LIKE 'M%' AND nazwisko like 'K%'
and to_char(datapodpisania, 'yyyy-MM') = '2008-11';

--Poda� nazwiska i imiona klient�w oraz liczb� um�w podpisanych z rabatem powy�ej 3%.
SELECT nazwisko, imie, COUNT(*)
FROM klienci k, umowy u, opisyumow o
WHERE k.idklienta=u.idklienta AND u.idumowy=o.idumowy AND rabat>3
GROUP BY nazwisko, imie;

--Poda� nazw� firmy dealera, list� samochod�w (IdSamochodu) dealera uzupe�ni� z prawej strony znakami �*� do 10 znak�w, dat� jaka b�dzie za 7 miesi�cy od daty wstawienia samochod�w.
SELECT nazwafirmy, rpad(idsamochodu, 10, '*'), add_months(data_wstawienia, 7)
FROM dealerzy d, samochody s
WHERE d.iddealera=s.iddealera;


