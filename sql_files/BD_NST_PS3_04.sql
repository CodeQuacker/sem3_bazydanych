-- 1. Poda� nazwiska os�b, kt�rych pensja i stanowisko s� takie same jak p. Forda.
SELECT ename
FROM emp
WHERE sal = (SELECT sal FROM emp WHERE UPPER(ename) = 'FORD')
  AND job = (SELECT job FROM emp WHERE UPPER(ename) = 'FORD');

-- 2. Poda� nazwisko, stanowisko, wynagrodzenia os�b maj�cych pensj� > od p. Millera i < od p. Forda.
SELECT ename, job, sal
FROM emp
WHERE sal > (SELECT sal FROM emp WHERE UPPER(ename) = 'MILLER')
  AND sal < (SELECT sal FROM emp WHERE UPPER(ename) = 'FORD');

-- 3. Poda� nazwisko, stopie� wynagrodzenia os�b, kt�rych wynagrodzenie jest inne ni� wynosi �rednie wynagrodzenie os�b na stanowisku Clerk.
SELECT ename, grade, sal
FROM emp e,
     salgrade s
WHERE sal BETWEEN losal AND hisal
  AND NOT sal = (SELECT AVG(sal) FROM emp WHERE UPPER(job) = 'CLERK');
-- SELECT avg(sal) FROM emp where upper(job)='CLERK';

-- 4. Poda� nazwisko i wynagrodzenie zwierzchnika p. Adams.
SELECT ename, sal
FROM emp
WHERE empno = (SELECT mgr FROM emp WHERE UPPER(ename) = 'ADAMS');

-- 5. Poda� nazw� projektu realizowanego wi�cej razy ni� projekt o numerze 1001.
SELECT proname, COUNT(1)
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
GROUP BY proname
HAVING COUNT(1) > (
    SELECT COUNT(*)
    FROM implproject
    WHERE prono = 1001);

-- 6. Poda� nazwiska pracownik�w, kt�rzy brali wi�cej razy udzia� w realizacji projekt�w ni� kt�rykolwiek pracownik z departamentu 30 .
SELECT ename, deptno, COUNT(1)
FROM emp e,
     implemp i
WHERE e.empno = i.empno
GROUP BY ename, deptno
HAVING COUNT(*) > (
    SELECT MAX(COUNT(*))
    FROM emp e,
         implemp i
    WHERE e.empno = i.empno
      AND deptno = 30
    GROUP BY ename
);

-- 7. Poda� nazwisko, stanowisko, wynagrodzenie os�b zarabiaj�cych wi�cej ni� wynosz� �rednie zarobki na ich stanowisku.
SELECT ename, job, sal
FROM emp E1
WHERE sal > (
    SELECT AVG(sal)
    FROM emp E2
    WHERE E1.job = E2.job
);
-- SELECT job, avg(sal)
-- FROM EMP
-- GROUP BY job
-- ORDER BY 1;
--
-- SELECT job, sal
-- FROM emp
-- ORDER BY 1;

-- 8. Dla ka�dego departamentu poda� pracownika najwi�cej razy realizuj�cego projekty.
SELECT e.ename, dname, COUNT(*)
FROM emp e,
     dept d,
     implemp ie,
     IMPLPROJECT ip
WHERE ie.empno = e.empno
  AND d.deptno = e.deptno
  AND ie.impl = ip.impl
GROUP BY e.ename, dname
HAVING max(count(*));

SELECT *
FROM IMPLEMP;


-- 9. Poda� nazwy stanowisk na kt�rych pracuje nie mniej ni� 2 pracownik�w i nie wi�cej ni� 4.


-- 10. Poda� nazwy projekt�w, kt�rych warto�� realizacji by�a wi�ksza w 2008r ni� wynosi maksymalna realizacje projekt�w w roku 2009.


-- 11. Poda� nazwy projekt�w, dla kt�rych r�nica dat rozpocz�cia i zako�czenia projektu by�a wi�ksza lub r�wna 3 miesi�ce.


-- 12. Poda� nazwy departament�w z kt�rych pracownicy na stanowisku MANAGER realizowali najmniej projekt�w spo�r�d pracownik�w na stanowisku MANAGER.