SELECT TO_DATE(SYSDATE, 'yyyy-MM-dd')
FROM dual;
SELECT SYSDATE
FROM dual;

--1. Sprawdzi� dzia�anie funkcji matematycznych POWER, SQRT, ABS, MOD.
SELECT POWER(5, 4), SQRT(81), ABS(-50), MOD(4, 3)
FROM dual;

--2. Wy�wietli� wynagrodzenia pracownik�w podniesione do kwadratu.
SELECT POWER(sal, 2)
FROM emp;

--3. Jaka data b�dzie za 100 dni.
SELECT SYSDATE + 100
FROM dual;

--4. Ile miesi�cy pracuj� poszczeg�lni pracownicy.
SELECT ename, TRUNC(MONTHS_BETWEEN(SYSDATE, hiredate)) AS time, MONTHS_BETWEEN(SYSDATE, hiredate)
FROM emp;

--5. Jak� dat� b�dziemy mieli za 10 miesi�cy.
SELECT ADD_MONTHS(SYSDATE, 10)
FROM dual;
SELECT ADD_MONTHS(TO_DATE(SYSDATE, 'yy-MM-dd'), 10)
FROM dual;

--6. Poda� dat� ostatniego dnia bie��cego miesi�ca.
SELECT LAST_DAY(SYSDATE)
FROM dual;

--7. Poda� nazwiska, wynagrodzenia, stopnie wynagrodzenia pracownik�w
--zatrudnionych w 1980 roku.
SELECT ename, sal, grade, hiredate
FROM emp,
     salgrade
WHERE sal BETWEEN losal AND hisal
  AND hiredate >= TO_DATE('1980-01-01', 'yyyy-MM-dd')
  AND hiredate <= TO_DATE('1980-01-30', 'yyyy-MM-dd');

--8. Poda� nazwy projekt�w realizowanych w miesi�cu styczniu.
SELECT proname
FROM PROJECT P,
     IMPLPROJECT IP
WHERE P.Prono = IP.prono AND (TO_CHAR(IP.start_date, 'MM') = 1 OR TO_CHAR(IP.END_DATE, 'MM') = 1)
   OR (TO_CHAR(IP.START_date, 'YY') < TO_CHAR(IP.end_date, 'YY'));

--9. Poda� nazwiska pracownik�w realizuj�cych projekty w pierszym kwartale 2008
--roku.
SELECT ename, start_date, end_date
FROM emp E,
     IMPLEMP IM,
     IMPLPROJECT IP
WHERE E.empno = IM.empno
  AND IM.impl = IP.impl;

--10. Pobra� 4 pierwsze rekordy z tabeli z wide�kami wynagrodze�. Sprawdzi�
--wp�yw klauzuli Order by na wynik zapytania.
SELECT *
FROM salgrade
WHERE rownum < 5
ORDER BY losal DESC;


--11. Zamieni� wszystkie literki E w imionach pracownik�w na a przy pomocy
--funkcji translate.
SELECT ename, TRANSLATE(ename, 'E', 'a')
FROM emp;

--12. Uzupe�ni� z prawej strony wynik kolumny dname znakami x do 15 znak�w w
--polu, kolumn� loc uzupe�ni� z lewej strony znakiem�-�.
SELECT dname, RPAD(dname, 15, 'x'), LPAD(dname, 1, '-')
FROM dept;

--13. Dla ka�dego pracownika poda� lokalizacj� jego departamentu z pomini�tym
--ostatnim znakiem
SELECT ename, loc, SUBSTR(loc, 1, LENGTH(loc) - 1)
FROM emp,
     dept
WHERE emp.deptno = dept.deptno;