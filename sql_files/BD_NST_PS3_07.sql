-- 1. Poda� list� pracownik�w nie realizuj�cych projekty ze stopniem zaszeregowania powy�ej 3.
SELECT ename,
       ie.impl
FROM emp e,
     implemp ie,
     salgrade sg
WHERE e.sal BETWEEN losal AND hisal
  AND grade > 3
  AND e.empno = ie.empno (+)
  AND ie.impl IS NULL;

SELECT ename,
       ie.impl
FROM emp e
         INNER JOIN salgrade sg ON (e.sal BETWEEN losal AND hisal)
         LEFT OUTER JOIN implemp ie ON (e.empno = ie.empno)
WHERE grade > 3
  AND ie.impl IS NULL;

SELECT e.empno, e.ename
FROM emp e,
     salgrade s
WHERE e.sal BETWEEN s.losal AND s.hisal
  AND grade > 3
  AND NOT EXISTS(SELECT 1 FROM implemp im WHERE e.empno = im.empno);

-- 2. Poda� nazwy projekt�w, kt�re by�y ponownie realizowane po up�ywie czasu kr�tszym ni� dwa miesi�ce.
SELECT DISTINCT p.prono,
                p.proname,
                ip2.prono,
                ip1.start_date,
                ip2.start_date
FROM implproject ip1
         INNER JOIN project p ON (p.prono = ip1.prono)
         INNER JOIN implproject ip2 ON (ip2.prono = ip1.prono)
WHERE ROUND(MONTHS_BETWEEN(ip1.start_date, ip2.start_date)) < 2
  AND ip1.start_date != ip2.start_date;

-- 3. Poda� nazwiska pracownik�w, realizuj�cych projekty w pierwszym kwartale 2009 roku. Uwzgl�dni� tylko te projekty przy realizacji kt�rych uczestniczy�o co najmniej dw�ch pracownik�w.
SELECT e.ename
FROM emp e,
     implemp ie,
     implproject ip
WHERE e.empno = ie.empno
  AND ie.impl = ip.impl
  AND TO_CHAR(ip.start_date, 'q') = 1
  AND EXTRACT(YEAR FROM ip.start_date) = 2008
  AND ip.impl IN (
    SELECT impl
    FROM implemp
    GROUP BY impl
    HAVING COUNT(1) >= 2
);

SELECT e.ename
FROM emp e
         INNER JOIN implemp ie USING (empno)
         INNER JOIN implproject ip USING (impl)
WHERE TO_CHAR(ip.start_date, 'q') = 1
  AND EXTRACT(YEAR FROM ip.start_date) = 2008
  AND impl IN (
    SELECT impl
    FROM implemp
    GROUP BY impl
    HAVING COUNT(1) >= 2
);

-- 4. Dla ka�dej nazwy departamentu poda� jego bud�et. W obliczeniach uwzgl�dni� kwot� na p�ace i premie pracownik�w oraz wynagrodzenie z tytu�u uczestnictwa w projekcie. Wynagrodzenie pracownika uczestnicz�cego w projekcie oblicza si� jako 5% bud�etu projektu pomno�one przez liczb� dni realizacji i podzielone prze liczb� realizuj�cych pracownik�w.
SELECT dname, SUM(e.sal + NVL(e.comm, 0)) + NVL(b.bonus, 0) + (0.05 * h.budget * h.id) / g.c) BUDZET
FROM dept d, emp e, bonus b
    (SELECT e.empno, e.ename, im.impl
    FROM emp e, impl
    );
--ZDJECIE

-- 5. Poda� list� pracownik�w, dat� zatrudnienia i wstawion� gwiazdk� w wierszu pracownika najwcze�niej zatrudnionego.
SELECT e1.empno, e1.ename, e1.hiredate || ' '
FROM emp e1
WHERE e1.hiredate != (SELECT MIN(e.hiredate) FROM emp e)
UNION
SELECT e2.empno, e2.ename, e2.hiredate || ' *'
FROM emp e2
WHERE e2.hiredate = (SELECT MIN(e.hiredate) FROM emp e)

-- 6 U�ywaj�c odwo�ania do kolumny empno w kolumnie mgr i z��czenie zewn�trzne w po��czeniu z funkcj� NVL pobra� informacje o pracownikach po lewej stronie pionowej kreski, a informacje o szefie po prawej.
SELECT e1.ename, '|', e2.ename
FROM emp e1
         LEFT JOIN emp e2 ON e1.mgr = e2.empno;

-- 7. Na podstawie zadania 6 utworzy� podzapytanie 3-poziomowe.
SELECT e1.ename, '|', e2.ename, '|', e3.ename
FROM emp e1
         LEFT JOIN emp e2 ON e1.mgr = e2.empno
         LEFT JOIN emp e3 ON e2.mgr = e3.empno;

-- 8. Na podstawie zadania 6 utworzy� podzapytanie 4-poziomowe.
SELECT e1.ename             e1,
       '|',
       NVL(e2.ename, '---') e2,
       '|',
       NVL(e3.ename, '---') e3,
       '|',
       NVL(e4.ename, '---') e4
FROM emp e1
         LEFT JOIN emp e2 ON e1.mgr = e2.empno
         LEFT JOIN emp e3 ON e2.mgr = e3.empno
         LEFT JOIN emp e4 ON e3.mgr = e4.empno;

-- 9. U�ywaj�c CONNECT BY PRIOR i START WITH wybra�:
-- a) pracownika o numerze 7902 i jego zwierzchnik�w.
SELECT e.empno, e.ename, e.mgr
FROM emp e
START WITH e.empno = 7902
CONNECT BY PRIOR e.mgr = e.empno;

-- b) pracownika o numerze 7839 i jego podw�adnych.
SELECT e.empno, e.ename, e.mgr
FROM emp e
START WITH e.empno = 7839
CONNECT BY PRIOR e.empno = e.mgr;

-- 10. Korzystaj�c z pseudokolumny LEVEL wskaza� poziom w�z�a w hierarchii, wybieraj�c:
-- a) pracownika o numerze 7521 i jego zwierzchnik�w.
-- b) pracownika o numerze 7499 i jego podw�adnych.