-- 1. Kt�rych pracownik�w departamenty mieszcz� si� w Dallas.
-- 2. Poda� nazwiska pracownik�w zatrudnionych w departamencie 10 na stanowiskach wyst�puj�cych w departamencie 30.
SELECT ename
FROM emp
WHERE deptno=10 and job in (
    SELECT DISTINCT job
    FROM emp
    WHERE deptno=30
    );

SELECT ename
FROM emp e1
WHERE deptno=10
AND EXISTS (
    SELECT empno
    FROM emp e2
    WHERE e2.deptno = 30 AND e1.job=e2.job
    );

-- 3. Kt�rzy pracownicy dzia�u 20 s� zatrudnieni na stanowiskach nie wyst�puj�cych w dziale 10.
SELECT ename
FROM emp
WHERE deptno=20 AND job NOT IN (
    SELECT DISTINCT job
    FROM emp
    WHERE deptno=10
);

-- 4. Kto zarabia wi�cej ni� kt�rakolwiek osoba na stanowisku Manager.
SELECT ename
FROM emp
WHERE sal > (
    SELECT min(sal)
    FROM emp
    WHERE initcap(job)='Manager'
    );
-- WHERE sal > ANY (
--     SELECT sal
--     FROM emp
--     WHERE initcap(job)='Manager'
--     );

-- 5. Poda� nazwiska pracownik�w z najwi�kszym wynagrodzeniem w firmie.
SELECT ename
FROM emp
WHERE sal = (
    SELECT max(sal)
    FROM emp
);
-- WHERE sal >= ALL (
--     SELECT sal
--     FROM emp
-- );

-- 6. Kt�rzy pracownicy z dzia�u 30 brali wi�cej razy udzia� w realizacji projekt�w od pracownik�w z dzia�u 20.


-- 7. Poda� nazwiska pracownik�w maj�cych podw�adnych tylko z lokalizacji departamentu w Chicago.
-- 8. Poda� nazwy departament�w z kt�rych nikt nie bra� udzia�u w realizacji projektu.
-- 9. Poda� nazwiska i stanowiska os�b, zatrudnionych w departamentach w kt�rych r�nica mi�dzy maksymalnymi a minimalnymi zarobkami jest wi�ksza lub r�wna �rednim zarobkom w departamencie.
-- 10. Poda� nazwisko, stanowisko os�b zatrudnionych w departamentach, dla kt�rych liczba realizowanych projekt�w jest wi�ksza od liczby zrealizowanych projekt�w w kt�rymkolwiek z departament�w podanych jako parametr (przynajmniej dw�ch).
-- 11. Poda� parami nazwiska os�b, dla kt�rych r�nica stopnia wynagrodzenia wynosi 2 lub 3.
-- 12. Poda� nazwy projekt�w realizowanych w 2010 roku, przy realizacji kt�rych brali udzia� pracownicy z departamentu o nazwie RESEARCH.
-- 13. Poda� nazwy projekt�w realizowanych przez najwi�ksz� liczb� pracownik�w.
-- 14. Dla ka�dego departamentu poda� nazwisko pracownika bior�cego udzia� w realizacji najwi�kszej liczby projekt�w.