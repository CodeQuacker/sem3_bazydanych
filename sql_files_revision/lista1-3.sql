-- Lista 1
--1. Za pomoc� polecenia DESC wy�wietli� struktury tabel: EMP, DEPT, SALGRADE.
DESC emp;

DESC dept;

DESC salgrade;

--2. Wy�wietli� wszystkie dane z tabel (2 sposoby).
SELECT *
FROM emp;

SELECT comm,
       deptno,
       empno,
       ename,
       hiredate,
       job,
       mgr,
       sal
FROM emp;

--3. Poda� nazwy departament�w.
SELECT DISTINCT dname
FROM dept;

--4. Poda� wszystkie, wzajemnie r�ne stanowiska pracy.
SELECT DISTINCT job
FROM emp;

--5. Poda� numery i nazwiska os�b zarabiaj�cych 2500 oraz nie wi�cej ni� 3200.
SELECT empno,
       ename
FROM emp
WHERE sal BETWEEN 2500 AND 3200;

--6. Poda� nazwiska i daty zatrudnienia pracownik�w na stanowiskach: SALESMAN, MANAGER, CLERK.
SELECT ename,
       hiredate
FROM emp
WHERE LOWER(job) IN ('salesman', 'manager', 'clerk');

--7. Dla ka�dego pracownika poda� sum� jego p�acy i zarobk�w.
SELECT ename,
       sal + NVL(comm, 0)
FROM emp;

--8. Poda� dane o pracownikach z departament�w 10 i 20 w kolejno�ci alfabetycznej wg nazwisk i malej�cej wg departamentu.
SELECT ename,
       deptno
FROM emp
WHERE deptno = 10
   OR deptno = 20
ORDER BY 1,
         2 DESC;

SELECT ename,
       deptno
FROM emp
WHERE deptno IN (10, 20)
ORDER BY 1,
         2 DESC;

--9. Wybra� informacje o pracownikach, kt�rzy nie posiadaj� zwierzchnika.
SELECT ename
FROM emp
WHERE mgr IS NULL;

--10. Poda� numer, nazwisko, stanowisko i wynagrodzenie pracownik�w na stanowisku SALESMAN z zarobkami powy�ej 1500 lub na stanowisku CLERK.
SELECT empno,
       ename,
       job,
       sal
FROM emp
WHERE LOWER(job) = 'salesman'
    AND sal > 1500
   OR LOWER(job) = 'clerk';

--11. Poda� nazwiska, nazwy departament�w pracownik�w, kt�rych departamenty nie mieszcz� si� w Chicago. Doda� alias dla pola nazwisko i dwucz�onowy dla nazwy departamentu.
SELECT ename "Nazwisko",
       dname "Nazwa departamentu"
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND LOWER(d.loc) != 'chicago';

--12. Poda� nazwiska pracownik�w, kt�rych zarobki s� wi�ksze ni� przynajmniej jednego pracownika z departamentu 10.
SELECT e1.ename
FROM emp e1,
     emp e2
WHERE e1.sal > e2.sal
  AND e2.deptno = 10;

--13. Poda� nazwiska pracownik�w, kt�rych zarobki powi�kszone o 25% przekraczaj� 3000.
SELECT ename,
       sal,
       sal * 1.25
FROM emp
WHERE sal * 1.25 > 3000;

--14. Ile miesi�cy pracuj� poszczeg�lni pracownicy.
SELECT ename,
       hiredate,
       TRUNC(MONTHS_BETWEEN(SYSDATE, hiredate)) "Ile miesiecy"
FROM emp;

--15. Poda� odpowied�, w postaci zdania, kt�ry pracownik nie posiada zwierzchnika.
SELECT INITCAP(ename)
           || ' nie posiada zwierzchnika.'
FROM emp
WHERE mgr IS NULL;

--16. Dla ka�dego pracownika poda� d�ugo�� jego nazwiska.
SELECT ename,
       LENGTH(ename) "Dlugosc nazwiska"
FROM emp;

--17. Poda� nazwiska pracownik�w zapisane z du�ej litery a stanowiska zapisane ma�ymi literami.
SELECT LOWER(ename),
       UPPER(job)
FROM emp;

--18. Poda� trzy sposoby wyszukania pracownika o nazwisku rozpoczynaj�cym si� od liter BL.
SELECT ename
FROM emp
WHERE ename LIKE 'BL%';

SELECT ename
FROM emp
WHERE SUBSTR(ename, 1, 2) = 'BL';

SELECT ename
FROM emp
WHERE INSTR(ename, 'BL') = 1;

-- Lista 2
--1. Sprawdzi� dzia�anie funkcji matematycznych POWER, SQRT, ABS, MOD.
SELECT POWER(2, 10),
       SQRT(100),
       ABS(- 23),
       MOD(11, 4)
FROM dual;

--2. Wy�wietli� wynagrodzenia pracownik�w podniesione do kwadratu.
SELECT POWER(sal, 2)
FROM emp;

--3. Jaka data b�dzie za 100 dni.
SELECT SYSDATE + 100
FROM dual;

--4. Ile miesi�cy pracuj� poszczeg�lni pracownicy.
SELECT ename,
       TRUNC(MONTHS_BETWEEN(SYSDATE, NVL(hiredate, SYSDATE))) "Ile pacuje miesiecy"
FROM emp;


--5. Jak� dat� b�dziemy mieli za 10 miesi�cy.
SELECT ADD_MONTHS(SYSDATE, 10)
FROM dual;

--6. Poda� dat� ostatniego dnia bie��cego miesi�ca.
SELECT LAST_DAY(SYSDATE)
FROM dual;

SELECT LAST_DAY(TO_DATE('11-10-2020', 'DD-MM-YYYY'))
FROM dual;

--7. Poda� nazwiska, wynagrodzenia, stopnie wynagrodzenia pracownik�w zatrudnionych w 1980 roku.
SELECT ename,
       sal,
       grade,
       hiredate
FROM emp,
     salgrade
WHERE sal BETWEEN losal AND hisal
  AND TO_CHAR(hiredate, 'YYYY') = 1980;

SELECT ename,
       sal,
       grade,
       hiredate
FROM emp,
     salgrade
WHERE sal BETWEEN losal AND hisal
  AND EXTRACT(YEAR FROM hiredate) = 1980;

--8. Poda� nazwy projekt�w realizowanych w miesi�cu styczniu.
SELECT proname,
       start_date,
       end_date
FROM project p,
     implproject i
WHERE p.prono = i.prono
  AND EXTRACT(MONTH FROM start_date) = 1
  AND EXTRACT(MONTH FROM end_date) = 1
  AND EXTRACT(YEAR FROM start_date) = EXTRACT(YEAR FROM end_date);

--9. Poda� nazwiska pracownik�w realizuj�cych projekty w pierszym kwartale 2008 roku.
SELECT ename,
       start_date,
       end_date
FROM emp e,
     implemp ie,
     implproject ip
WHERE e.empno = ie.empno
  AND ie.impl = ip.impl
  AND TO_CHAR(start_date, 'Q') = 1
  AND TO_CHAR(end_date, 'Q') = 1
  AND EXTRACT(YEAR FROM start_date) = 2008
  AND EXTRACT(YEAR FROM end_date) = 2008;

--10. Pobra� 4 pierwsze rekordy z tabeli z wide�kami wynagrodze�. Sprawdzi� wp�yw klauzuli Order by na wynik zapytania.
SELECT grade,
       losal,
       hisal
FROM salgrade
WHERE ROWNUM <= 4;

SELECT grade,
       losal,
       hisal
FROM salgrade
WHERE ROWNUM <= 4
ORDER BY 2 DESC;


--11. Zamieni� wszystkie literki E w imionach pracownik�w na a przy pomocy funkcji translate.
SELECT TRANSLATE(ename, 'EC', 'XV')
FROM emp;

--12. Uzupe�ni� z prawej strony wynik kolumny dname znakami x do 15 znak�w w polu, kolumn� loc uzupe�ni� z lewej strony znakiem�-�.
SELECT RPAD(dname, 15, 'x'),
       '-' || loc
FROM dept;

SELECT RPAD(dname, 15, 'x'),
       LPAD(loc, 15, '-')
FROM dept;

--13. Dla ka�dego pracownika poda� lokalizacj� jego departamentu z pomini�tym ostatnim znakiem.
SELECT ename,
       loc,
       SUBSTR(loc, 1, LENGTH(loc) - 1) loc2
FROM emp,
     dept;

-- Lista 3
--1. Poda� liczb� pracownik�w w tabeli EMP.
SELECT COUNT(ename)
FROM emp;

--2. Poda� min, max, avg, sum pensj� w firmie.
SELECT MIN(sal),
       MAX(sal),
       ROUND(AVG(sal), 2),
       SUM(sal)
FROM emp;

--3. Obliczy� ilu pracownik�w jest kierownikami.
SELECT COUNT(1)
FROM emp
WHERE LOWER(job) = 'manager';

SELECT COUNT(DISTINCT mgr)
FROM emp;

--4. Poda� min, max pensj� dla pracownik�w z departamentu Research.
SELECT MIN(sal),
       MAX(sal)
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND LOWER(dname) = 'research';

--5. Poda� liczb� pracownik�w ze stopniem wynagrodzenia 1.
SELECT COUNT(ename)
FROM emp e,
     salgrade s
WHERE grade = 1
  AND sal BETWEEN losal AND hisal;

--6. Poda� liczb� pracownik�w z premi� oraz sumaryczn� warto�� premii pracownik�w z departamentu z lokalizacj� w Dallas.
SELECT COUNT(comm),
       SUM(comm)
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND LOWER(loc) = 'dallas'
  AND comm IS NOT NULL;

--7. Poda� min, max pensj� dla ka�dego stanowiska.
SELECT MIN(sal),
       MAX(sal),
       job
FROM emp
GROUP BY job;

--8. Obliczy� ilu pracownik�w posiada poszczeg�lne stopnie wynagrodzenia.
SELECT COUNT(1),
       grade
FROM emp e,
     salgrade s
WHERE e.sal BETWEEN s.losal AND s.hisal
GROUP BY grade;

--9. Poda� dla ka�dego departamentu sum� zarobk�w i premii jego pracownik�w.
SELECT dname,
       SUM(sal),
       SUM(NVL(comm, 0))
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
GROUP BY dname,
         d.deptno;

--10. Poda� stanowiska, dla kt�rych max zarobki s� <2500.
SELECT job,
       MAX(sal)
FROM emp
GROUP BY job
HAVING MAX(sal) < 2500;

--11. Poda� nazwy departament�w dla kt�rych liczba pracownik�w na poszczeg�lnych stanowiskach przekracza 2 osoby.
SELECT dname,
       job,
       COUNT(1)
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
GROUP BY d.deptno,
         dname,
         job
HAVING COUNT(1) > 2;

--12. Poda� stopie� wynagrodzenia z liczb� pracownik�w od 2 do 5.
SELECT grade,
       COUNT(1)
FROM emp e,
     salgrade s
WHERE sal BETWEEN losal AND hisal
GROUP BY grade
HAVING COUNT(1) BETWEEN 2 AND 5;

--13. Poda� nazwy stanowisk dla kt�rych r�nica mi�dzy maksymalnymi i minimalnymi zarobkami nie przekracza 1000.
SELECT job
FROM emp
GROUP BY job
HAVING MAX(sal) - MIN(sal) <= 1000;

--14. Poda� stanowiska, na kt�rych pracuje 3 pracownik�w ze stopniem wynagrodzenia 1.
SELECT job
FROM emp e,
     salgrade s
WHERE sal BETWEEN losal AND hisal
  AND grade = 1
GROUP BY job
HAVING COUNT(1) = 3;

--15. Kt�re stanowiska w kt�rych dzia�ach s� obsadzone przez dw�ch lub wi�cej pracownik�w. Uporz�dkowa� wg liczby pracownik�w.
SELECT e.job,
       d.dname,
       COUNT(1)
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
GROUP BY e.job,
         d.deptno,
         d.dname
HAVING COUNT(1) >= 2
ORDER BY 3;

SELECT ename
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND job = 'SALESMAN'
  AND dname = 'SALES';

--16. Poda� sum� bonus�w uzyskanych przez pracownik�w departamentu 10.
SELECT SUM(NVL(bonus, 0))
FROM emp e,
     bonus b,
     dept d
WHERE e.empno = b.empno
  AND d.deptno = e.deptno
  AND d.deptno = 10;

--17. Dla ka�dej nazwy projektu poda� liczb� realizacji w 2015 roku.
SELECT proname,
       COUNT(1)
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
  AND TO_CHAR(start_date, 'YYYY') = 2015
  AND TO_CHAR(end_date, 'YYYY') = 2015
GROUP BY proname,
         p.prono;

--18. Dla ka�dego departamentu poda� nazwy realizowanych projekt�w i liczb� realizacji.
SELECT d.dname,
       p.proname,
       COUNT(1)
FROM dept d,
     emp e,
     implemp ie,
     implproject ip,
     project p
WHERE d.deptno = e.deptno
  AND e.empno = ie.empno
  AND ip.impl = ie.impl
  AND p.prono = ip.prono
GROUP BY d.deptno,
         d.dname,
         p.prono,
         p.proname;

--19. Dla ka�dego stanowiska w departamencie poda� maksymalne bonusy pracownik�w.
SELECT e.job,
       d.dname,
       MAX(b.bonus)
FROM emp e,
     dept d,
     bonus b
WHERE d.deptno = e.deptno
  AND e.empno = b.empno
GROUP BY e.job,
         d.deptno,
         d.dname;

--20. Dla ka�dej nazwy projektu poda� minimaln� sumaryczn� warto�� bud�etu realizacji.
SELECT p.proname,
       SUM(budget)
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
GROUP BY p.proname;