-- Lista 4
--1. Poda� nazwiska os�b, kt�rych pensja i stanowisko s� takie same jak p. Forda.
SELECT ename
FROM emp
WHERE (sal, job) IN (
    SELECT sal,
           job
    FROM emp
    WHERE LOWER(ename) = 'ford'
)
  AND LOWER(ename) != 'ford';

--2. Poda� nazwisko, stanowisko, wynagrodzenia os�b maj�cych pensj� > od p. Millera i < od p. Forda.
SELECT ename,
       job,
       sal
FROM emp
WHERE sal > (
    SELECT sal
    FROM emp
    WHERE LOWER(ename) = 'miller'
)
  AND sal < (
    SELECT sal
    FROM emp
    WHERE LOWER(ename) = 'ford'
);

--3. Poda� nazwisko, stopie� wynagrodzenia os�b, kt�rych wynagrodzenie jest inne ni� wynosi �rednie wynagrodzenie os�b na stanowisku Clerk.
SELECT e.ename,
       s.grade
FROM emp e,
     salgrade s
WHERE e.sal BETWEEN losal AND hisal
  AND sal != (
    SELECT AVG(sal)
    FROM emp
    WHERE LOWER(job) = 'clerk'
);

--4. Poda� nazwisko i wynagrodzenie zwierzchnika p. Adams.
SELECT ename,
       sal
FROM emp
WHERE empno = (
    SELECT mgr
    FROM emp
    WHERE LOWER(ename) = 'adams'
);

--5. Poda� nazw� projektu realizowanego wi�cej razy ni� projekt o numerze 1001.
SELECT p.prono,
       p.proname,
       COUNT(1)
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
GROUP BY p.prono,
         p.proname
HAVING COUNT(p.prono) > (
    SELECT COUNT(1)
    FROM implproject ip2
    WHERE ip2.prono = 1001
);

--6. Poda� nazwiska pracownik�w, kt�rzy brali wi�cej razy udzia� w realizacji projekt�w ni� kt�rykolwiek pracownik z departamentu 30.
SELECT e1.ename,
       COUNT(1)
FROM emp e1,
     implemp ie1
WHERE e1.empno = ie1.empno
GROUP BY e1.empno,
         e1.ename
HAVING COUNT(1) > (
    SELECT MIN(COUNT(1))
    FROM emp e2,
         implemp ie2
    WHERE e2.empno = ie2.empno
      AND e2.deptno = 30
    GROUP BY e2.empno,
             e2.ename
);

SELECT COUNT(1)
FROM emp e2,
     implemp ie2
WHERE e2.empno = ie2.empno
  AND e2.deptno = 30
GROUP BY e2.empno,
         e2.ename;

--7. Poda� nazwisko, stanowisko, wynagrodzenie os�b zarabiaj�cych wi�cej ni� wynosz� �rednie zarobki na ich stanowisku.
SELECT ename,
       job,
       sal
FROM emp e1
WHERE sal > (
    SELECT AVG(sal)
    FROM emp e2
    WHERE e1.job = e2.job
);

--8. Dla ka�dego departamentu poda� pracownika najwi�cej razy realizuj�cego projekty.
SELECT d1.dname,
       e1.ename,
       COUNT(1)
FROM dept d1,
     emp e1,
     implemp ie1
WHERE d1.deptno = e1.deptno
  AND e1.empno = ie1.empno
GROUP BY d1.deptno,
         d1.dname,
         e1.empno,
         e1.ename
HAVING COUNT(1) = (
    SELECT MAX(COUNT(1))
    FROM emp e2,
         implemp ie2
    WHERE e2.empno = ie2.empno
      AND d1.deptno = e2.deptno
    GROUP BY e2.empno
)
ORDER BY e1.ename;

--9. Poda� nazwy stanowisk na kt�rych pracuje nie mniej ni� 2 pracownik�w i nie wi�cej ni� 4.
SELECT job,
       COUNT(1)
FROM emp
GROUP BY job
HAVING COUNT(1) BETWEEN 2 AND 4;

--10. Poda� nazwy projekt�w, kt�rych warto�� realizacji by�a wi�ksza w 2008r ni� wynosi maksymalna realizacje projekt�w w roku 2009.
SELECT p.proname
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
  AND EXTRACT(YEAR FROM ip.start_date) = 2008
  AND EXTRACT(YEAR FROM ip.end_date) = 2008
GROUP BY p.prono,
         p.proname
HAVING SUM(budget) > (
    SELECT MAX(SUM(p1.budget))
    FROM project p1,
         implproject ip1
    WHERE p1.prono = ip1.prono
      AND TO_CHAR(ip1.start_date, 'YYYY') = 2009
      AND TO_CHAR(ip1.end_date, 'YYYY') = 2000
    GROUP BY p1.prono
);

--11. Poda� nazwy projekt�w, dla kt�rych r�nica dat rozpocz�cia i zako�czenia projektu by�a wi�ksza lub r�wna 3 miesi�ce.
SELECT p.proname,
       ip.start_date,
       ip.start_date
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
  AND MONTHS_BETWEEN(ip.end_date, ip.start_date) >= 3;

--12. Poda� nazwy departament�w z kt�rych pracownicy na stanowisku MANAGER realizowali najmniej projekt�w spo�r�d pracownik�w na stanowisku MANAGER.
SELECT d.dname,
       e.ename,
       COUNT(1)
FROM dept d,
     emp e,
     implemp ie
WHERE d.deptno = e.deptno
  AND e.empno = ie.empno
  AND LOWER(e.job) = 'manager'
GROUP BY d.deptno,
         d.dname,
         e.empno,
         e.ename
HAVING COUNT(1) = (
    SELECT MIN(COUNT(1))
    FROM dept d,
         emp e,
         implemp ie
    WHERE d.deptno = e.deptno
      AND e.empno = ie.empno
      AND LOWER(e.job) = 'manager'
    GROUP BY d.deptno,
             e.empno
);