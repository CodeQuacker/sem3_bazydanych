-- 1. Za pomoc� polecenia DESC wy�wietli� struktury tabel: EMP, DEPT, SALGRADE.
-- 2. Wy�wietli� wszystkie dane z tabel (2 sposoby).
SELECT *
FROM EMP;

-- 3. Poda� nazwy departament�w.
SELECT dname
FROM dept;

-- 4. Poda� wszystkie, wzajemnie r�ne stanowiska pracy.
SELECT UNIQUE job
FROM EMP;

-- 5. Poda� numery i nazwiska os�b zarabiaj�cych 2500 oraz nie wi�cej ni� 3200.
SELECT empno, ename
FROM emp
WHERE sal BETWEEN 2500 AND 3000;

-- 6. Poda� nazwiska i daty zatrudnienia pracownik�w na stanowiskach : SALESMAN, MANAGER, CLERK.
SELECT ename, hiredate
FROM EMP
WHERE job IN ('SALESMAN', 'MANAGER', 'CLERK');

-- 7. Dla ka�dego pracownika poda� sum� jego p�acy i zarobk�w.
SELECT ename, sal + NVL(comm, 0)
FROM emp;

-- 8. Poda� dane o pracownikach z departament�w 10 i 20 w kolejno�ci alfabetycznej wg nazwisk i malej�cej wg departamentu.
SELECT *
FROM EMP
WHERE deptno IN (10, 20)
ORDER BY ename ASC, deptno DESC;

-- 9. Wybra� informacje o pracownikach, kt�rzy nie posiadaj� zwierzchnika.
SELECT *
FROM EMP
WHERE mgr IS NULL;

-- 10. Poda� numer, nazwisko, stanowisko i wynagrodzenie pracownik�w na stanowisku SALESMAN z zarobkami powy�ej 1500 lub na stanowisku CLERK.
SELECT empno, ename, job, sal
FROM emp
WHERE job = 'SALESMAN' AND sal > 1500
   OR job = 'CLERK';

-- 11. Poda� nazwiska, nazwy departament�w pracownik�w, kt�rych departamenty nie mieszcz� si� w Chicago. Doda� alias dla pola nazwisko i dwucz�onowy dla nazwy departamentu.
SELECT ename Nazwisko, dname AS NAZWA_DEPARTAMENTU
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND loc != 'CHICAGO';

-- 12. Poda� nazwiska pracownik�w, kt�rych zarobki s� wi�ksze ni� przynajmniej jednego pracownika z departamentu 10.
SELECT UNIQUE e1.empno, e1.ename
FROM emp e1,
     emp e2
WHERE e1.sal > e2.sal
  AND e2.deptno = 10;

-- 13. Poda� nazwiska pracownik�w, kt�rych zarobki powi�kszone o 25% przekraczaj� 3000.
SELECT ename
FROM emp
WHERE sal * 1.25 > 3000;

-- 14. Ile miesi�cy pracuj� poszczeg�lni pracownicy.
SELECT TRUNC(MONTHS_BETWEEN(CURRENT_DATE, hiredate), 0) AS MONTHS
FROM emp;

-- 15. Poda� odpowied�, w postaci zdania, kt�ry pracownik nie posiada zwierzchnika.
SELECT 'Pracownik ' || RPAD(ename, 10) || ' nie posiada zwierzchnika'
FROM emp
WHERE mgr IS NULL;

-- 16. Dla ka�dego pracownika poda� d�ugo�� jego nazwiska.
SELECT ename, LENGTH(ename) Dlugosc
FROM emp;

-- 17. Poda� nazwiska pracownik�w zapisane z du�ej litery a stanowiska zapisane ma�ymi literami.
SELECT INITCAP(ename), LOWER(job)
FROM emp;

-- 18. Poda� trzy sposoby wyszukania pracownika o nazwisku rozpoczynaj�cym si� od liter BL.
SELECT *
FROM EMP
WHERE ename LIKE 'BL%';