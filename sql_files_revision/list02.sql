-- 1. Sprawdzi� dzia�anie funkcji matematycznych POWER, SQRT, ABS, MOD.
SELECT sal, ABS(sal)
FROM emp;

-- 2. Wy�wietli� wynagrodzenia pracownik�w podniesione do kwadratu.
SELECT POWER(sal, 2)
FROM emp;

-- 3. Jaka data b�dzie za 100 dni.
SELECT CURRENT_DATE + 100
FROM dual;

-- 4. Ile miesi�cy pracuj� poszczeg�lni pracownicy.
SELECT ename, TRUNC(MONTHS_BETWEEN(CURRENT_DATE, hiredate), 0) Months
FROM emp;

-- 5. Jak� dat� b�dziemy mieli za 10 miesi�cy.
SELECT ADD_MONTHS(CURRENT_DATE, 10)
FROM dual;

-- 6. Poda� dat� ostatniego dnia bie��cego miesi�ca.
SELECT LAST_DAY(CURRENT_DATE)
FROM dual;

-- 7. Poda� nazwiska, wynagrodzenia, stopnie wynagrodzenia pracownik�w zatrudnionych w 1980 roku.
SELECT ename, sal, sal + NVL(comm, 0), grade
FROM EMP,
     SALGRADE
WHERE sal BETWEEN losal AND hisal
  AND TO_CHAR(hiredate, 'YYYY') = 1980;

-- 8. Poda� nazwy projekt�w realizowanych w miesi�cu styczniu.
SELECT proname
FROM IMPLPROJECT ip,
     PROJECT p
WHERE ip.prono = p.prono
  AND (EXTRACT(MONTH FROM start_date) = 1
    OR EXTRACT(MONTH FROM end_date) = 1
    OR EXTRACT(MONTH FROM start_date) < EXTRACT(MONTH FROM end_date));

-- 9. Poda� nazwiska pracownik�w realizuj�cych projekty w pierszym kwartale 2008 roku.
SELECT ename
FROM emp e,
     IMPLEMP ie,
     IMPLPROJECT ip
WHERE e.empno = ie.empno
  AND ie.impl = ip.impl
  AND (EXTRACT(MONTH FROM start_date) >= 1
    OR EXTRACT(MONTH FROM end_date) >= 3)
  AND EXTRACT(YEAR FROM start_date) = 2008
  AND EXTRACT(YEAR FROM end_date) = 2008;

-- 10. Pobra� 4 pierwsze rekordy z tabeli z wide�kami wynagrodze�. Sprawdzi� wp�yw klauzuli Order by na wynik zapytania.
SELECT *
FROM SALGRADE
WHERE ROWNUM <= 4
ORDER BY 1 ASC;

-- 11. Zamieni� wszystkie literki E w imionach pracownik�w na a przy pomocy funkcji translate.
SELECT TRANSLATE(ename, 'E', 'A')
FROM emp;

-- 12. Uzupe�ni� z prawej strony wynik kolumny dname znakami x do 15 znak�w w polu, kolumn� loc uzupe�ni� z lewej strony znakiem�-�.
SELECT RPAD(dname, 15, 'x'), LPAD(loc, LENGTH(loc) + 1, '-')
FROM dept;

-- 13. Dla ka�dego pracownika poda� lokalizacj� jego departamentu z pomini�tym ostatnim znakiem.
SELECT ename, rpad(loc, length(loc)-1)
FROM emp e, dept d
WHERE d.deptno = e.deptno;