-- 1. Poda� liczb� pracownik�w w tabeli EMP.
SELECT COUNT(*)
FROM emp;

-- 2. Poda� min, max, avg, sum pensj� w firmie.
SELECT MIN(sal), MAX(sal), TRUNC(AVG(sal), 2), SUM(sal)
FROM emp;

-- 3. Obliczy� ilu pracownik�w jest kierownikami.
SELECT COUNT(*)
FROM EMP
WHERE job = 'MANAGER';

-- 4. Poda� min, max pensj� dla pracownik�w z departamentu Research.
SELECT MIN(sal), MAX(sal), COUNT(*)
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND dname = 'RESEARCH';

-- 5. Poda� liczb� pracownik�w ze stopniem wynagrodzenia 1.
SELECT COUNT(*)
FROM emp e,
     SALGRADE s
WHERE sal BETWEEN losal AND hisal
  AND grade = 1;

-- 6. Poda� liczb� pracownik�w z premi� oraz sumaryczn� warto�� premii pracownik�w z departamentu z lokalizacj� w Dallas.

-- 7. Poda� min, max pensj� dla ka�dego stanowiska.
-- 8. Obliczy� ilu pracownik�w posiada poszczeg�lne stopnie wynagrodzenia.
-- 9. Poda� dla ka�dego departamentu sum� zarobk�w i premii jego pracownik�w.
-- 10. Poda� stanowiska, dla kt�rych max zarobki s� <2500.
-- 11. Poda� nazwy departament�w dla kt�rych liczba pracownik�w na poszczeg�lnych stanowiskach przekracza 2 osoby.
-- 12. Poda� stopie� wynagrodzenia z liczb� pracownik�w od 2 do 5.
-- 13. Poda� nazwy stanowisk dla kt�rych r�nica mi�dzy maksymalnymi i minimalnymi zarobkami nie przekracza 1000.
-- 14. Poda� stanowiska, na kt�rych pracuje 3 pracownik�w ze stopniem wynagrodzenia 1.
-- 15. Kt�re stanowiska w kt�rych dzia�ach s� obsadzone przez dw�ch lub wi�cej pracownik�w. Uporz�dkowa� wg liczby pracownik�w.
-- 16. Poda� sum� bonus�w uzyskanych przez pracownik�w departamentu 10.
-- 17. Dla ka�dej nazwy projektu poda� liczb� realizacji w 2015 roku.
-- 18. Dla ka�dego departamentu poda� nazwy realizowanych projekt�w i liczb� realizacji.
-- 19. Dla ka�dego stanowiska w departamencie poda� maksymalne bonusy pracownik�w.
-- 20. Dla ka�dej nazwy projektu poda� minimaln� sumaryczn� warto�� bud�etu realizacji.