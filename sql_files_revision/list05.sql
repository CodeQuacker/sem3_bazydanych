-- 1. Kt�rych pracownik�w departamenty mieszcz� si� w Dallas.
SELECT ename
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND loc = 'DALLAS';

-- 2. Poda� nazwiska pracownik�w zatrudnionych w departamencie 10 na stanowiskach wyst�puj�cych w departamencie 30.
SELECT e.ename, e.deptno, e.job
FROM emp e,
     dept d
WHERE d.deptno = e.deptno
  AND (d.deptno = 10
    OR e.job IN (SELECT UNIQUE job FROM emp WHERE deptno = 30));

-- 3. Kt�rzy pracownicy dzia�u 20 s� zatrudnieni na stanowiskach nie wyst�puj�cych w dziale 10.
SELECT ename
FROM emp e
WHERE deptno = 20
  AND job NOT IN (SELECT UNIQUE job FROM emp WHERE deptno = 10);

-- 4. Kto zarabia wi�cej ni� kt�rakolwiek osoba na stanowisku Manager.
SELECT ename
FROM emp
WHERE sal > (SELECT MAX(sal) FROM emp WHERE job = 'MANAGER');
SELECT job, MAX(sal)
FROM EMP
GROUP BY job
ORDER BY 2 DESC;

-- rekord testowy
SELECT *
FROM emp
WHERE empno = 1113;
INSERT INTO emp
VALUES (1113, 'RICHMAN', 'VICEPREZ', 7698, CURRENT_DATE, 8000, 1000, 10);
DELETE
FROM emp
WHERE empno = 1113;

-- 5. Poda� nazwiska pracownik�w z najwi�kszym wynagrodzeniem w firmie.
SELECT ename, sal + NVL(comm, 0)
FROM emp e
WHERE sal + NVL(comm, 0) = (SELECT MAX(sal + NVL(comm, 0)) FROM emp);

-- 6. Kt�rzy pracownicy z dzia�u 30 brali wi�cej razy udzia� w realizacji projekt�w od pracownik�w z dzia�u 20.
SELECT e.ename, COUNT(1)
FROM emp e,
     implemp ie
WHERE deptno = 30
  AND e.empno = ie.empno
GROUP BY ename
HAVING COUNT(1) > (SELECT MIN(COUNT(ename))
                   FROM emp e1,
                        implemp ie1
                   WHERE e1.deptno = 20
                     AND e1.empno = ie1.empno
                   GROUP BY ename);

-- 7. Poda� nazwiska pracownik�w maj�cych podw�adnych tylko z lokalizacji departamentu w Chicago.
SELECT ename
FROM emp e
WHERE empno IN (SELECT mgr
                FROM emp e1,
                     dept d1
                WHERE d1.deptno = e1.deptno
                  AND d1.loc = 'CHICAGO')
  AND empno NOT IN (SELECT mgr
                    FROM emp e2,
                         dept d2
                    WHERE d2.deptno = e2.deptno
                      AND d2.loc != 'CHICAGO');

-- 8. Poda� nazwy departament�w z kt�rych nikt nie bra� udzia�u w realizacji projektu.
SELECT d.dname
FROM dept d
WHERE deptno NOT IN (SELECT UNIQUE d1.deptno
                     FROM dept d1,
                          emp e1,
                          implemp ie1
                     WHERE d1.deptno = e1.deptno
                       AND e1.empno = ie1.empno);

--
-- SELECT *
-- FROM DEPT;
-- SELECT *
-- FROM EMP;
-- SELECT *
-- FROM IMPLEMP;
--
-- INSERT INTO dept
-- VALUES (50, 'EMPTY', 'NOLOC');
-- INSERT INTO emp
-- VALUES (2, 'EMPTY', 'NOLOC', NULL, NULL, NULL, NULL, 50);
--
-- DELETE
-- FROM dept
-- WHERE deptno = 50;
-- DELETE
-- FROM emp
-- WHERE empno = 2;

-- 9. Poda� nazwiska i stanowiska os�b, zatrudnionych w departamentach w kt�rych r�nica mi�dzy maksymalnymi a minimalnymi zarobkami jest wi�ksza lub r�wna �rednim zarobkom w departamencie.

-- 10. Poda� nazwisko, stanowisko os�b zatrudnionych w departamentach, dla kt�rych liczba realizowanych projekt�w jest wi�ksza od liczby zrealizowanych projekt�w w kt�rymkolwiek z departament�w podanych jako parametr (przynajmniej dw�ch).

-- 11. Poda� parami nazwiska os�b, dla kt�rych r�nica stopnia wynagrodzenia wynosi 2 lub 3.

-- 12. Poda� nazwy projekt�w realizowanych w 2010 roku, przy realizacji kt�rych brali udzia� pracownicy z departamentu o nazwie RESEARCH.
SELECT *
FROM project;
SELECT proname
FROM project p,
     implproject ip
WHERE p.prono = ip.prono
  AND EXTRACT(YEAR FROM start_date) <= 2010
  AND EXTRACT(YEAR FROM end_date) >= 2010
  AND p.prono IN (SELECT ip1.prono
                  FROM implproject ip1
                           LEFT JOIN implemp ie1 USING (impl)
                           LEFT JOIN emp e1 USING (empno)
                           LEFT JOIN dept d1 USING (deptno)
                  WHERE d1.dname = 'RESEARCH');

-- 13. Poda� nazwy projekt�w realizowanych przez najwi�ksz� liczb� pracownik�w.
SELECT p.proname, COUNT(1)
FROM project p
         LEFT JOIN IMPLPROJECT ip USING (prono)
         LEFT JOIN IMPLEMP ie USING (impl)
GROUP BY prono, proname
HAVING COUNT(1) = (SELECT MAX(COUNT(*))
                   FROM IMPLEMP ie1
                            LEFT JOIN IMPLPROJECT ip1 USING (impl)
                   GROUP BY prono);

SELECT prono, COUNT(*)
FROM IMPLPROJECT
GROUP BY prono;


-- 14. Dla ka�dego departamentu poda� nazwisko pracownika bior�cego udzia� w realizacji najwi�kszej liczby projekt�w.

