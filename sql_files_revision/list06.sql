-- 1. Dla ka�dego projektu poda� liczb� jego realizacji.
-- 2. Poda� list� pracownik�w oraz liczb� realizowanych przez nich projekt�w.
-- 3. Poda� nazwy departament�w z kt�rych bierze udzia� w projektach najwi�cej pracownik�w.
-- 4. Poda� w tym samym wierszu nazw� projektu z najwi�kszym oraz najmniejszym bud�etem.
-- 5. Dla ka�dego departamentu poda� jaki procent stanowi� pracownicy oraz ich zarobki, bior�c pod uwag� wszystkie departamenty.
-- 6. Poda� nazwiska trzech pracownik�w najcz�ciej realizuj�cych projekty.
-- 7. Poda� dwa najrzadziej realizowane projekty.
-- 8. Poda� warto�� realizowanych projekt�w w 2008 roku.
-- 9. Poda� warto�ci realizowanych projekt�w w poszczeg�lnych latach realizacji.
-- 10. Poda� nazw� projektu realizowanego w najkr�tszym czasie.
-- 11. Poda� nazwy projekt�w nie realizowanych.
-- 12. W jednym wierszu poda� sumaryczne zarobki pracownik�w na poszczeg�lnych stanowiskach oraz liczb� pracownik�w nie posiadaj�cych bonus�w uwzgl�dniaj�c stanowisko i departament.

SELECT d.deptno, dname, loc, ename, job
  FROM dept d, emp e
  WHERE d.deptno = e.deptno (+);

SELECT d.deptno, dname, loc, ename, job
FROM dept d , emp e
WHERE e.deptno (+) = d.deptno;

SELECT LEVEL, emp.empno, emp.ename, emp.deptno,
       emp.mgr
FROM emp
CONNECT BY PRIOR EMPNO=MGR
START WITH ename='JONES'
ORDER BY level;

SELECT lpad(level-1,level*4-3,'===>') distance,
       empno, ename , deptno, mgr
FROM Emp
CONNECT BY PRIOR mgr=empno
START WITH empno=7876;